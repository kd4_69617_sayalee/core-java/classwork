import java.util.StringTokenizer;

public class StringToken {

	public static void main(String[] args) {
		//public static void main(String[] args) {
			// String name = "rohan is from cdac";
			// String name = "rohan,is,from,cdac";
			String name = "https://www.sunbeaminfo.com/placements";
			System.out.println(name);

			// StringTokenizer stok = new StringTokenizer(name);
			// StringTokenizer stok = new StringTokenizer(name, ",");
			// StringTokenizer stok = new StringTokenizer(name, "://./");
			StringTokenizer stok = new StringTokenizer(name, "://./", true);

			String token;
			while (stok.hasMoreTokens()) {
				token = stok.nextToken();
				System.out.println(token);
			}

		}

	

}
