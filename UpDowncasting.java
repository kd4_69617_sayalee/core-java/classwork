class Parent
{
	private int n1;
	private int n2;
	private int n3;
	private int n4;
	
	public Parent()
	{
		System.out.println("Inside Parent:Parameterless Constructor");
		this.n1=0;
		this.n2=0;
		this.n3=0;
		this.n4=0;
		
	}
	public void f1()
	{
		System.out.println("Inside Parent:f1()");
	}
	public void f2()
	{
		System.out.println("Inside Parent:f2()");
	}
}
class Child extends Parent 
{
	public Child() {
		System.out.println("Inside Child:Parameterless constructor");
		}
	public void f3()
	{
		System.out.println("Inside child:f3()");
	}
}
public class UpDowncasting {

	public static void main(String[] args) {
		Parent p =new Child();
		Child c1=(Child)p;
		c1.f1();
		c1.f2();
		
		c1.f3();
		
		
	}

//	private static Child p() {
//		// TODO Auto-generated method stub
//		return null;
//	}

}
